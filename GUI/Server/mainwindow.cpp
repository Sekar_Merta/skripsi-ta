#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(663, 431);

    ui->statusBar->showMessage("Ready.");

    // Menu Bar actions
    connect( ui->actionExit, SIGNAL (triggered()), this, SLOT (closeApp()) );

    // Radio Button actions
    connect( ui->rbRunning, SIGNAL (clicked()), this, SLOT (runPortKnocking()) );
    connect( ui->rbPaused, SIGNAL (clicked()), this, SLOT (pausePortKnocking()) );
    connect( ui->rbStopped, SIGNAL (clicked()), this, SLOT (stopPortKnocking()) );
}

void MainWindow::closeApp()
{
    ui->txLogs->appendHtml("<b>Closing application...</b>");
    ui->statusBar->showMessage("Stopping port knocking...");
    qApp->exit();
}

void MainWindow::runPortKnocking()
{
    ui->txLogs->appendHtml("<i>Running port knocking...</i>");
    ui->statusBar->showMessage("Port Knocking is now running...");
}

void MainWindow::pausePortKnocking()
{
    ui->txLogs->appendHtml("<i>Pausing port knocking...</i>");
    ui->statusBar->showMessage("Port Knocking has been paused...");
}

void MainWindow::stopPortKnocking()
{
    ui->txLogs->appendHtml("<i>Stopping port knocking...</i>");
    ui->statusBar->showMessage("Port Knocking has been stopped...");
}

MainWindow::~MainWindow()
{
    delete ui;
}
