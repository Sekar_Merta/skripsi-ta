#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(663, 265);

    ui->statusBar->showMessage("Ready.");

    // Menu Bar actions
    connect( ui->actionExit, SIGNAL (triggered()), this, SLOT (closeApp()) );

    // Connect functions

}

void MainWindow::closeApp()
{
    ui->txLogs->appendHtml("<b>Closing application...</b>");
    ui->statusBar->showMessage("Stopping port knocking...");
    qApp->exit();
}

void MainWindow::connectPortKnocking()
{
    ui->txLogs->appendHtml("<i>Connecting to server port knocking...</i>");
    ui->statusBar->showMessage("Port Knocking is now connecting...");
}

MainWindow::~MainWindow()
{
    delete ui;
}
